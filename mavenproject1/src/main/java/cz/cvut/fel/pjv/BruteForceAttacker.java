package cz.cvut.fel.pjv;

public class BruteForceAttacker extends Thief {
    
    //private char[] possabilities = getCharacters();
    private boolean running = true;
    
    private char[] normalAlgorithem(char[] password, int position)
    {    
            int attempt = 0;
            while(attempt<getCharacters().length)
            {
               password[position] = getCharacters()[attempt];
               password =  guesser(password, (position+1));
               if(running)
               {
                   attempt++;
               }
               else
               {
                   break;
               }
            }
            return password;
    }
    
    private char[] lastAlgorithem(char[] password, int position)
    {
        int attempt = 0;
        while(attempt<getCharacters().length){
            password[position] = getCharacters() [attempt];
            //System.out.println(password);
            if (tryOpen(password)==true)
            {
                running = false;
                break;
            }
            else
            {
                //System.out.println("Here");
                attempt++;
            }
        }
        return password;
    }
    
    
    private char[] guesser(char[] password, int position) //Enter 0 for position and attempt
    {
        //System.out.println(getCharacters()[0]);
        //System.out.println("Got here");
        int attempt = 0;
        password[position] = getCharacters() [attempt];//This gives me the null pointer error
        if(position<password.length-1) //Code for all but the last possition of the array
        {
           password = normalAlgorithem(password, position);
        }
        else//Specific code for the last position
        {
            password = lastAlgorithem(password, position);
        }
        return password;
    }
        
    
    @Override
    public void breakPassword(int sizeOfPassword) 
    {
        char[] password = new char[sizeOfPassword];
        //System.out.println(password[1]);//it seems to be an empty line, not null
        password = guesser(password, 0);
    }

    public BruteForceAttacker() {
    }
}
